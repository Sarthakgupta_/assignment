// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest( myArray )
{
  var biggest = 0;
  biggest.MIN_VALUE;
  console.log(biggest);
  var nextBiggest = 0;
  nextBiggest.MIN_VALUE;
  for(var index=0; index < myArray.length ; index++ )
  {
    if( myArray[ index ] >= biggest )
    {
       nextBiggest = biggest;
       biggest = myArray[ index ];
    }
    if(myArray[ index ] > nextBiggest && myArray[index] < biggest )
    {            
       nextBiggest = myArray[ index ];
    }
  }
  return nextBiggest;  
}

// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
  var count = {};
  var strArray=[];
  var str=string.split('');
  for(let index = 0 ; index < str.length ; index++ )
  {
    if(str[ index ] >= 'a' && str[ index ] <= 'z' )
    {
      strArray.push(str[ index ]);
    }
  }
  strArray.forEach( function( strElement ) { count[ strElement ] ? count[ strElement ]++ : count[ strElement ] = 1;
  } );
  return count;
}

// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatten) {
  var addObj={};
  for( var subObject in unflatten )
  {
	  if( typeof( unflatten[ subObject ] ) =='object' )
    {
		  var flattenObj = flatten( unflatten[ subObject] );
		  for ( let sub in flattenObj )
      {
			  addObj[ subObject +'.'+sub ] = flattenObj[ sub ];
			}
		}
		if( typeof( unflatten[ subObject ] ) !='object' )
    {
			addObj[ subObject ] = unflatten[ subObject ];
		}
	}
return addObj 
}

// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
function unflatten(flatObject) {
  // Write your code here
}

